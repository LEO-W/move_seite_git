function valSubmission(e) {
    /* DATA GATHERING */
    var fname = document.forms["contact-form"]["vorname"].value;
    var sname = document.forms["contact-form"]["nachname"].value;
    var mail = document.forms["contact-form"]["mail"].value;
    var age = document.forms["contact-form"]["alter"].value;
    var msg = document.forms["contact-form"]["nachricht"].value;
    var kurse = [];
    if ($(".select-1 select").val() != null) {
        kurse.push($(".select-1 select").val());
    }
    if ($(".select-2 select").val() != null) {
        kurse.push($(".select-2 select").val());
    }
    if ($(".select-3 select").val() != null) {
        kurse.push($(".select-3 select").val());
    }
    if ($(".select-4 select").val() != null) {
        kurse.push($(".select-4 select").val());
    }

    /* VALIDATION AND ERRORS */
    if (fname != null && fname != "" && isNaN(fname)) {
    } else {
        e.preventDefault(e);
        alert("Bitte gib deinen Vornamen an.");
        $(".form-line:nth-child(1) label:nth-child(1) input").css({"border":"1px solid red"});
    }

    if (sname != null && sname != "" && isNaN(sname)) {
    } else {
        e.preventDefault(e);
        alert("Bitte gib deinen Nachnamen an.");
        $(".form-line:nth-child(1) label:nth-child(2) input").css({"border":"1px solid red"});
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      }

    if (mail != null && mail != "" && isNaN(mail) && validateEmail(mail)) {
    } else {
        e.preventDefault(e);
        alert("Bitte gib eine korrekte E-Mail-Adresse an.");
        $(".form-line:nth-child(2) label:nth-child(1) input").css({"border":"1px solid red"});
    }

    if (age != null && age != "" && !isNaN(age)) {
        if (age > 120) {
            e.preventDefault(e);
            alert("Bitte gib dein richtiges Alter an");
        }
    } else {
        e.preventDefault(e);
        alert("Bitte gib dein richtiges Alter an");
        $(".form-line:nth-child(2) label:nth-child(2) input").css({"border":"1px solid red"});
    }

    if ($('.datenschutz').is(':checked')) {
    } else {
        e.preventDefault(e);
        alert("Bitte bestätige, dass du unsere Datenschutzerklärung gelesen hast. (Sofern du das noch nicht gemacht hast, kannst du das nachholen, indem du dem turquoisen Link folgst.)");
        $(".form-datenschutz").css({"text-decoration":" red underline wavy"});
    }

    if (msg == "" && kurse == "") {
        e.preventDefault(e);
        alert("Wir freuen uns, dass du uns kontaktieren willst, aber bitte schreibe uns dann doch eine Nachricht und/oder melde dich für einen oder mehrere unserer Workshops an.");
    }
}

$(document).ready(function() {

    $(".contact-form").submit(function(e){
        valSubmission(e);
    });

    var select_num = 1;

    $(document).on('click', ".select-1", function() { 
        if ($(".select-"+select_num+" select").val() != null) {
            addSelect();
            if ($(".select-1 select").val() == "Hip-Hop-Dance Workshop") {
                $(".select-2 select .o1").remove();
            } else if ($(".select-1 select").val() == "Liedtexte Schmiede") {
                $(".select-2 select .o2").remove();
            } else if ($(".select-1 select").val() == "Kompaktkurs: Videodreh und Schnitt") {
                $(".select-2 select .o3").remove();
            } else if ($(".select-1 select").val() == "Deutsch für Jugendliche") {
                $(".select-2 select .o4").remove();
            }
        } 
    });
    
    $(document).on('click', ".select-2", function() { 
        if ($(".select-"+select_num+" select").val() != null) {
            addSelect();
            if ($(".select-1 select").val() == "Hip-Hop-Dance Workshop") {
                $(".select-3 select .o1").remove();
            } else if ($(".select-1 select").val() == "Liedtexte Schmiede") {
                $(".select-3 select .o2").remove();
            } else if ($(".select-1 select").val() == "Kompaktkurs: Videodreh und Schnitt") {
                $(".select-3 select .o3").remove();
            } else if ($(".select-1 select").val() == "Deutsch für Jugendliche") {
                $(".select-3 select .o4").remove();
            }
            if ($(".select-2 select").val() == "Hip-Hop-Dance Workshop") {
                $(".select-3 select .o1").remove();
            } else if ($(".select-2 select").val() == "Liedtexte Schmiede") {
                $(".select-3 select .o2").remove();
            } else if ($(".select-2 select").val() == "Kompaktkurs: Videodreh und Schnitt") {
                $(".select-3 select .o3").remove();
            } else if ($(".select-2 select").val() == "Deutsch für Jugendliche") {
                $(".select-3 select .o4").remove();
            }
        } 
    });

    $(document).on('click', ".select-3", function() { 
        if ($(".select-"+select_num+" select").val() != null) {
            addSelect();
            if ($(".select-1 select").val() == "Hip-Hop-Dance Workshop") {
                $(".select-4 select .o1").remove();
            } else if ($(".select-1 select").val() == "Liedtexte Schmiede") {
                $(".select-4 select .o2").remove();
            } else if ($(".select-1 select").val() == "Kompaktkurs: Videodreh und Schnitt") {
                $(".select-4 select .o3").remove();
            } else if ($(".select-1 select").val() == "Deutsch für Jugendliche") {
                $(".select-4 select .o4").remove();
            }
            if ($(".select-2 select").val() == "Hip-Hop-Dance Workshop") {
                $(".select-4 select .o1").remove();
            } else if ($(".select-2 select").val() == "Liedtexte Schmiede") {
                $(".select-4 select .o2").remove();
            } else if ($(".select-2 select").val() == "Kompaktkurs: Videodreh und Schnitt") {
                $(".select-4 select .o3").remove();
            } else if ($(".select-2 select").val() == "Deutsch für Jugendliche") {
                $(".select-4 select .o4").remove();
            }
            if ($(".select-3 select").val() == "Hip-Hop-Dance Workshop") {
                $(".select-4 select .o1").remove();
            } else if ($(".select-3 select").val() == "Liedtexte Schmiede") {
                $(".select-4 select .o2").remove();
            } else if ($(".select-3 select").val() == "Kompaktkurs: Videodreh und Schnitt") {
                $(".select-4 select .o3").remove();
            } else if ($(".select-3 select").val() == "Deutsch für Jugendliche") {
                $(".select-4 select .o4").remove();
            }
        } 
    });

    function addSelect() {
        var new_num = select_num + 1;
            // alert(select_num);
            // alert($(".select-"+select_num+" select").val());
            $('<label class="workshop-selection select-'+new_num+'">Workshop '+new_num+' (optional)<br><select class="workshop" name="workshop"><option disabled selected value> -- Einen Workshop auswählen -- </option><option  class="o1">Hip-Hop-Dance Workshop</option><option  class="o2">Liedtexte Schmiede</option><option  class="o3">Kompaktkurs: Videodreh und Schnitt</option><option  class="o4">Deutsch für Jugendliche</option></select></label>').insertAfter(".select-"+select_num);

            select_num = new_num;
            // alert(select_num);
            return select_num;
    }
});