$(document).ready(function() {
    /* BURGERMENU */
    $(".burgermenu-container").hide();
    if ($(window).width() < 740 || $(document).width() < 740) {
        $(".burgermenu-container").show();
        $(".navi-list").hide();

        $(".burgermenu-container").click(function() {
            $(".navi-list").slideToggle();
        });
    } else if ($(window).width() < 770 && $(window).height() < 1080 || $(document).width() < 770 && $(document).height() < 1080) {
        $(".burgermenu-container").show();
        $(".navi-list").hide();

        $(".burgermenu-container").click(function() {      
            $(".navi-list").slideToggle();
        });
    } else if ($(window).width() < 830 && $(window).height() < 500 || $(document).width() < 830 && $(document).height() < 500) {
        $(".burgermenu-container").show();
        $(".navi-list").hide();

        $(".burgermenu-container").click(function() {
            $(".navi-list").slideToggle();
        });
    }

    /* SPECIAL LINKS HOVER */
    $(".special-link").mouseover(function() {
        $(this).find(".link-name").css({"background-color": "#ffff66"});
    });
    $(".special-link").mouseleave(function() {
        $(this).find(".link-name").css({"background-color": "#ffffff"});
    });

    /* BACK TO TOP */
    $(".back-to-top").hide();
    $(window).scroll(function(event) {
        if($(window).scrollTop() > 500) {
            $(".back-to-top").show();
        } else {
            $(".back-to-top").hide();
        }
    });

    /* KONTEXTBILDER */
    $(".kontext-bild img").hide();
    $(".kontext-bild img.kb1").show();
    var tid = setInterval(loopPic, 7000);
    var i = 0;
    function loopPic() {
        if (i == 0) {
            $(".kontext-bild img.kb4").fadeOut(1000);
        } else if (i == 1) {
            $(".kontext-bild img.kb2").fadeIn(1000);
        } else if (i == 2) {
            $(".kontext-bild img.kb3").fadeIn(1000);
            $(".kontext-bild img.kb2").fadeOut(1000);
        } else if (i == 3) {
            $(".kontext-bild img.kb4").fadeIn(1000);
            $(".kontext-bild img.kb3").fadeOut(1000);
            i = -1;
        }
        i = i + 1;
        return i;
    }
});