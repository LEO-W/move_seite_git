$(document).ready(function() {
    /* BUILD CALENDAR BASIC */

    /* CALENDAR MODE */
    $(".calendar-classic").hide();
    $(".cal-mode").click(function() {
        if($(this).hasClass("active")) {
            
        } else {
            $(".calendar-list").hide();
            $(".calendar-classic").hide();
            $(".cal-mode").removeClass("active");
            $(this).addClass("active");
            if($(this).hasClass("veranstaltungen")) {
                $(".calendar-list").show();
            } else if($(this).hasClass("kalenderansicht")) {
                $(".calendar-classic").show();
            }
        }
    });

    /* EVENT TAB FUNCTIONS */
    $(".event-tab-content").find(".et-age").hide();
    $(".event-tab-content").find(".et-slots").hide();
    $(".event-tab-content").find(".et-text").hide();
    $(".event-tab-content").find(".sign-up").hide();
    $(".event-tab").click(function() {
        if($(this).hasClass("open")) {
            $(this).find(".et-teaser").show();
            $(this).find(".et-age").hide();
            $(this).find(".et-slots").hide();
            $(this).find(".et-text").hide();
            $(this).find(".sign-up").hide();
            $(this).removeClass("open");
            $(this).find(".klapper").attr("src","assets/icons/angle-down-solid.svg");
        } else {
            $(".event-tab-content").find(".et-teaser").show();
            $(".event-tab-content").find(".et-age").hide();
            $(".event-tab-content").find(".et-slots").hide();
            $(".event-tab-content").find(".et-text").hide();
            $(".event-tab-content").find(".sign-up").hide();
            $(".event-tab-content").removeClass("open");
            $(".event-tab-content").find(".klapper").attr("src","assets/icons/angle-down-solid.svg");

            $(this).find(".et-teaser").hide();
            $(this).find(".et-age").show();
            $(this).find(".et-slots").show();
            $(this).find(".et-text").show();
            $(this).find(".sign-up").show();
            $(this).addClass("open");
            $(this).find(".klapper").attr("src","assets/icons/angle-up-solid.svg");
        }
    });

    /* CALENDAR VARIABLES / DATE VARIABLES */
    $currentYear = new Date().getFullYear();

	$monthNum = new Date().getMonth();
	var months = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'];
	$currentMonth = months[$monthNum];

	var weekday = ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'];
	$today = new Date();
	$turner = 1;
	$turner2 = 1;

	$selectYear = $currentYear;
    $selectMonth = 6;
    
    /* STANDARD FUNCTIONS */
    function back($month, $year) {
		$month = parseInt($month) - 1;
		if ($month<0) {
			$month = 11;
			$year = $year - 1;
        }
        if ($month != 6) {
            $(".event-tab").hide();
        } else {
            $(".event-tab").show();
        }
		$selectMonth = $month;
		$selectYear = $year;
		$turner = 1;
		$turner2 = 1;
        buildCalendar($selectMonth, $selectYear);
        colorItAll($selectMonth);
		return $selectMonth;
		return $selectYear;
	}

	$('.back').click(function() {
		back($selectMonth, $selectYear);
	});

	function forth($month, $year) {
		$month = parseInt($month) + 1;
		if ($month>11) {
			$month = 0;
			$year = $year + 1;
        }
        if ($month != 6) {
            $(".event-tab").hide();
        } else {
            $(".event-tab").show();
        }
		$selectMonth = $month;
		$selectYear = $year;
		$turner = 1;
		$turner2 = 1;
        buildCalendar($selectMonth, $selectYear);
        colorItAll($selectMonth);
		return $selectMonth;
		return $selectYear;
	}

	$('.forth').click(function() {
		forth($selectMonth, $selectYear);
	});

	// function today($month, $year) {
	// 	$selectMonth = $month;
	// 	$selectYear = $year;
	// 	$turner = 1;
	// 	$turner2 = 1;
	// 	buildCalendar($selectMonth, $selectYear);
	// 	return $selectMonth;
	// 	return $selectYear
	// }

	// $('.today').click(function() {
	// 	today($monthNum, $currentYear);
	// });

	function buildCalendar($month, $year) {
		// alert('month: '+$month+' und Year: '+$year);
		$firstDay = (new Date($year, $month)).getDay();
		// alert($weekday[$firstDay]);
		$toda = weekday[new Date().getDay()];
		// alert($toda);
		$daysInMonth = 32 - new Date($year, $month, 32).getDate();
		// alert('ym: '+$daysInMonth);

		$('.date p').html(months[$month]+' '+$year);

		$tbl = $('.date-body');
		$tbl.html("");

		// alert(weekday[$firstDay]);
		$date = 1;
		// alert('date: '+$date);
		for (var i = 0; i < 6	; i++) {
			var $row = document.createElement('tr');
			$row.classList.add('calendar-row');

			for (var j = 0; j < 7; j++) {
				if ($turner == true && $firstDay != 0) {
						j++;

					if (i === 0 && j < $firstDay) {
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						if ($turner2 == true && $firstDay == 6) {
							$cell.remove();
							$turner2 = 0;
						} else if ($turner2 == true && $firstDay == 4) {
							// alert(weekday[$firstDay]);
							$cell.remove();
							j = j - 1;
							$turner2 = 0;
						} else if ($turner2 == true && $firstDay == 2) {
							$cell.remove();
							j = j - 1;
							$turner2 = 0;
						}
					} else if ($date > $daysInMonth) {
						break;
					} else {
						var $cell = document.createElement('td');
                        $cell.classList.add('calendar-cell');
                        
						var $cellVal = document.createTextNode($date);
						if ($date === $today.getDate() && $year === $today.getFullYear() && $month === $today.getMonth()) {
	                    	$cell.classList.add("todayday");
	                	}
						$cell.append($cellVal);
						$row.append($cell);
						$date++;
						$turner = 0;
					}
				}
				
				if (i === 0 && j < $firstDay) {
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
					} else if (i === 0 && $firstDay == 0 && $turner2 == true) {
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						var $cell = document.createElement('td');
						$cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode('');
						$cell.append($cellVal);
						$row.append($cell);
						j = 5;
						$turner2 = 0;
					} else if ($date > $daysInMonth) {
						break;
					} else {
						var $cell = document.createElement('td');
                        $cell.classList.add('calendar-cell');
						var $cellVal = document.createTextNode($date);
						if ($date === $today.getDate() && $year === $today.getFullYear() && $month === $today.getMonth()) {
	                    	$cell.classList.add("todayday");
	                	}
						$cell.append($cellVal);
						$row.append($cell);
						$date++;
					}
				// var $cell = document.createElement('td');
				
			}

			$tbl.append($row);
			// alert('i: '+i);
		}
	}

    buildCalendar($selectMonth, $selectYear);
    
    function colorItAll($month) {
        if ($month != 6) {
            
        } else {
		/* FIRST ROW */
			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(1)").html('<p class="cell-num">1</p><div class="hip-hop cell-button-top">O</div><div class="cell-button-bottom">V</div>');
			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(1) .cell-button-top").css({"background":"orange"});
			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(1) .cell-button-bottom").css({"background":"red"});
		   
			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(2)").html('<p class="cell-num">2</p><div class="hip-hop cell-button-top">O</div><div class="cell-button-bottom">V</div>');
			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(2) .cell-button-top").css({"background":"orange"});
			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(2) .cell-button-bottom").css({"background":"red"});

            $(".calendar-row:nth-child(1) .calendar-cell:nth-child(3)").html('<p class="cell-num">3</p><div class="hip-hop cell-button-top-top">O</div><div class="cell-button-middle">V</div><div class="cell-button-bottom-bottom">E</div>');
			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(3) .cell-button-top-top").css({"background":"orange"});
			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(3) .cell-button-middle").css({"background":"red"});
			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(3) .cell-button-bottom-bottom").css({"background":"violet"});

			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(4)").html('<p class="cell-num">4</p><div class="cell-button">V</div>');
			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(4) .cell-button").css({"background":"red"});

			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(5)").html('<p class="cell-num">5</p><div class="cell-button-top">V</div><div class="kino cell-button-bottom">E</div>');
			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(5) .cell-button-top").css({"background":"red"});
			$(".calendar-row:nth-child(1) .calendar-cell:nth-child(5) .cell-button-bottom").css({"background":"pink"});

		/* SECOND ROW */
			$(".calendar-row:nth-child(2) .calendar-cell:nth-child(1)").html('<p class="cell-num">8</p><div class="cell-button">V</div>');
			$(".calendar-row:nth-child(2) .calendar-cell:nth-child(1) .cell-button").css({"background":"blue"});

			$(".calendar-row:nth-child(2) .calendar-cell:nth-child(2)").html('<p class="cell-num">9</p><div class="cell-button">V</div>');
			$(".calendar-row:nth-child(2) .calendar-cell:nth-child(2) .cell-button").css({"background":"blue"});

			$(".calendar-row:nth-child(2) .calendar-cell:nth-child(5)").html('<p class="cell-num">12</p><div class="kino cell-button-bottom-bottom">E</div><div class="cell-button-middle">E</div><div class="cell-button-top-top">E</div>');
			$(".calendar-row:nth-child(2) .calendar-cell:nth-child(5) .cell-button-bottom-bottom").css({"background":"pink"});
			$(".calendar-row:nth-child(2) .calendar-cell:nth-child(5) .cell-button-middle").css({"background":"orangered"});
			$(".calendar-row:nth-child(2) .calendar-cell:nth-child(5) .cell-button-top-top").css({"background":"purple"});

			$(".calendar-row:nth-child(2) .calendar-cell:nth-child(6)").html('<p class="cell-num">9</p><div class="cell-button">E</div>');
			$(".calendar-row:nth-child(2) .calendar-cell:nth-child(6) .cell-button").css({"background":"purple"});

			$(".calendar-row:nth-child(2) .calendar-cell:nth-child(7)").html('<p class="cell-num">9</p><div class="cell-button">E</div>');
			$(".calendar-row:nth-child(2) .calendar-cell:nth-child(7) .cell-button").css({"background":"purple"});
			
		/* THIRD ROW */
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(1)").html('<p class="cell-num">15</p><div class="cell-button-top-top">M</div><div class="cell-button-middle">V</div><div class="cell-button-bottom-bottom">E</div>');
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(1) .cell-button-top-top").css({"background":"green"});
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(1) .cell-button-middle").css({"background":"red"});
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(1) .cell-button-bottom-bottom").css({"background":"purple"});
			
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(2)").html('<p class="cell-num">16</p><div class="cell-button-top-top">M</div><div class="cell-button-middle">V</div><div class="cell-button-bottom-bottom">E</div>');
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(2) .cell-button-top-top").css({"background":"green"});
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(2) .cell-button-middle").css({"background":"red"});
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(2) .cell-button-bottom-bottom").css({"background":"purple"});
			
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(3)").html('<p class="cell-num">17</p><div class="cell-button-top-top">M</div><div class="cell-button-middle">V</div><div class="cell-button-bottom-bottom"><div class="cell-button-bbl">E</div><div class="cell-button-bbr">E</div></div>');
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(3) .cell-button-top-top").css({"background":"green"});
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(3) .cell-button-middle").css({"background":"red"});
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(3) .cell-button-bottom-bottom .cell-button-bbl").css({"background":"greenyellow"});
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(3) .cell-button-bottom-bottom .cell-button-bbr").css({"background":"purple"});
			
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(4)").html('<p class="cell-num">18</p><div class="cell-button-top-top">M</div><div class="cell-button-middle">V</div><div class="cell-button-bottom-bottom">E</div>');
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(4) .cell-button-top-top").css({"background":"green"});
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(4) .cell-button-middle").css({"background":"red"});
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(4) .cell-button-bottom-bottom").css({"background":"purple"});
			
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(5)").html('<p class="cell-num">19</p><div class="cell-button-top-top">M</div><div class="cell-button-middle"><div class="cell-button-bbl">V</div><div class="cell-button-bbr">E</div></div><div class="cell-button-bottom-bottom"><div class="cell-button-bbl">E</div><div class="cell-button-bbr">E</div></div>');
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(5) .cell-button-top-top").css({"background":"green"});
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(5) .cell-button-middle .cell-button-bbl").css({"background":"red"});
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(5) .cell-button-middle .cell-button-bbr").css({"background":"blueviolet"});
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(5) .cell-button-bottom-bottom .cell-button-bbl").css({"background":"pink"});
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(5) .cell-button-bottom-bottom .cell-button-bbr").css({"background":"purple"});

			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(6)").html('<p class="cell-num">9</p><div class="cell-button">E</div>');
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(6) .cell-button").css({"background":"purple"});

			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(7)").html('<p class="cell-num">9</p><div class="cell-button">E</div>');
			$(".calendar-row:nth-child(3) .calendar-cell:nth-child(7) .cell-button").css({"background":"purple"});
			
		/* FOURTH ROW */
			$(".calendar-row:nth-child(4) .calendar-cell:nth-child(1)").html('<p class="cell-num">22</p><div class="cell-button">M</div>');
			$(".calendar-row:nth-child(4) .calendar-cell:nth-child(1) .cell-button").css({"background":"green"});
			
			$(".calendar-row:nth-child(4) .calendar-cell:nth-child(2)").html('<p class="cell-num">23</p><div class="cell-button">M</div>');
			$(".calendar-row:nth-child(4) .calendar-cell:nth-child(2) .cell-button").css({"background":"green"});
			
			$(".calendar-row:nth-child(4) .calendar-cell:nth-child(3)").html('<p class="cell-num">24</p><div class="cell-button">M</div>');
			$(".calendar-row:nth-child(4) .calendar-cell:nth-child(3) .cell-button").css({"background":"green"});
			
			$(".calendar-row:nth-child(4) .calendar-cell:nth-child(4)").html('<p class="cell-num">25</p><div class="cell-button">M</div>');
			$(".calendar-row:nth-child(4) .calendar-cell:nth-child(4) .cell-button").css({"background":"green"});
			
			$(".calendar-row:nth-child(4) .calendar-cell:nth-child(5)").html('<p class="cell-num">26</p><div class="cell-button-top">M</div><div class="kino cell-button-bottom">E</div>');
			$(".calendar-row:nth-child(4) .calendar-cell:nth-child(5) .cell-button-top").css({"background":"green"});
			$(".calendar-row:nth-child(4) .calendar-cell:nth-child(5) .cell-button-bottom").css({"background":"pink"});
        }
    }

    colorItAll($selectMonth);
});